# Doppler-Go

An unofficial Go wrapper for the [Doppler API](https://docs.doppler.com/reference#download)


## Installation

Using Go Modules in your project (There will be a `go.mod` file in its root if it is).

Otherwise to begin using Go Modules use the following command:

```
go mod init
```

Call `go get` the package into a project.

```sh
go get -u gitlab.com/avocagrow/doppler-go"
```

Then reference doppler-go in a Go program with import 

```go
import (
    "github.com/theoriginalstove/doppler-go"
)
```

